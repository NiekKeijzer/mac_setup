# MacOS Ansible

## Usage

```
ansible-playbook main.yaml -i inventory.ini --ask-become-pass
```

## TODO

- [x] Install Brew
- [x] Install applications with Brew
- [ ] Clone dotfiles repo
- [ ] Configure installed applications
- [ ] Add install script for completely hands off procedure
